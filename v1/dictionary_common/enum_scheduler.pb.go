// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.13.0
// source: v1/dictionary_common/enum_scheduler.proto

package dictionary_common

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type SchedulerTaskStatus int32

const (
	SchedulerTaskStatus_SHTS_UNSPECIFIED SchedulerTaskStatus = 0
	SchedulerTaskStatus_SHTS_WAITING     SchedulerTaskStatus = 1
	SchedulerTaskStatus_SHTS_ERROR       SchedulerTaskStatus = 2
	SchedulerTaskStatus_SHTS_LOCKED      SchedulerTaskStatus = 3
	SchedulerTaskStatus_SHTS_WORK        SchedulerTaskStatus = 4
	SchedulerTaskStatus_SHTS_DELETE      SchedulerTaskStatus = 5
)

// Enum value maps for SchedulerTaskStatus.
var (
	SchedulerTaskStatus_name = map[int32]string{
		0: "SHTS_UNSPECIFIED",
		1: "SHTS_WAITING",
		2: "SHTS_ERROR",
		3: "SHTS_LOCKED",
		4: "SHTS_WORK",
		5: "SHTS_DELETE",
	}
	SchedulerTaskStatus_value = map[string]int32{
		"SHTS_UNSPECIFIED": 0,
		"SHTS_WAITING":     1,
		"SHTS_ERROR":       2,
		"SHTS_LOCKED":      3,
		"SHTS_WORK":        4,
		"SHTS_DELETE":      5,
	}
)

func (x SchedulerTaskStatus) Enum() *SchedulerTaskStatus {
	p := new(SchedulerTaskStatus)
	*p = x
	return p
}

func (x SchedulerTaskStatus) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SchedulerTaskStatus) Descriptor() protoreflect.EnumDescriptor {
	return file_v1_dictionary_common_enum_scheduler_proto_enumTypes[0].Descriptor()
}

func (SchedulerTaskStatus) Type() protoreflect.EnumType {
	return &file_v1_dictionary_common_enum_scheduler_proto_enumTypes[0]
}

func (x SchedulerTaskStatus) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SchedulerTaskStatus.Descriptor instead.
func (SchedulerTaskStatus) EnumDescriptor() ([]byte, []int) {
	return file_v1_dictionary_common_enum_scheduler_proto_rawDescGZIP(), []int{0}
}

type SchedulerTaskType int32

const (
	SchedulerTaskType_SHTT_UNSPECIFIED       SchedulerTaskType = 0
	SchedulerTaskType_SHTT_ONCE              SchedulerTaskType = 1
	SchedulerTaskType_SHTT_PERIODIC          SchedulerTaskType = 2
	SchedulerTaskType_SHTT_PERIODIC_BY_MODEL SchedulerTaskType = 3
)

// Enum value maps for SchedulerTaskType.
var (
	SchedulerTaskType_name = map[int32]string{
		0: "SHTT_UNSPECIFIED",
		1: "SHTT_ONCE",
		2: "SHTT_PERIODIC",
		3: "SHTT_PERIODIC_BY_MODEL",
	}
	SchedulerTaskType_value = map[string]int32{
		"SHTT_UNSPECIFIED":       0,
		"SHTT_ONCE":              1,
		"SHTT_PERIODIC":          2,
		"SHTT_PERIODIC_BY_MODEL": 3,
	}
)

func (x SchedulerTaskType) Enum() *SchedulerTaskType {
	p := new(SchedulerTaskType)
	*p = x
	return p
}

func (x SchedulerTaskType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SchedulerTaskType) Descriptor() protoreflect.EnumDescriptor {
	return file_v1_dictionary_common_enum_scheduler_proto_enumTypes[1].Descriptor()
}

func (SchedulerTaskType) Type() protoreflect.EnumType {
	return &file_v1_dictionary_common_enum_scheduler_proto_enumTypes[1]
}

func (x SchedulerTaskType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SchedulerTaskType.Descriptor instead.
func (SchedulerTaskType) EnumDescriptor() ([]byte, []int) {
	return file_v1_dictionary_common_enum_scheduler_proto_rawDescGZIP(), []int{1}
}

type SchedulerTaskPriority int32

const (
	SchedulerTaskPriority_SHTP_UNSPECIFIED SchedulerTaskPriority = 0
	SchedulerTaskPriority_SHTP_CRITICAL    SchedulerTaskPriority = 1
	SchedulerTaskPriority_SHTP_IMPORTANT   SchedulerTaskPriority = 2
	SchedulerTaskPriority_SHTP_COMMON      SchedulerTaskPriority = 3
	SchedulerTaskPriority_SHTP_MINOR       SchedulerTaskPriority = 4
	SchedulerTaskPriority_SHTP_STUFF       SchedulerTaskPriority = 5
)

// Enum value maps for SchedulerTaskPriority.
var (
	SchedulerTaskPriority_name = map[int32]string{
		0: "SHTP_UNSPECIFIED",
		1: "SHTP_CRITICAL",
		2: "SHTP_IMPORTANT",
		3: "SHTP_COMMON",
		4: "SHTP_MINOR",
		5: "SHTP_STUFF",
	}
	SchedulerTaskPriority_value = map[string]int32{
		"SHTP_UNSPECIFIED": 0,
		"SHTP_CRITICAL":    1,
		"SHTP_IMPORTANT":   2,
		"SHTP_COMMON":      3,
		"SHTP_MINOR":       4,
		"SHTP_STUFF":       5,
	}
)

func (x SchedulerTaskPriority) Enum() *SchedulerTaskPriority {
	p := new(SchedulerTaskPriority)
	*p = x
	return p
}

func (x SchedulerTaskPriority) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SchedulerTaskPriority) Descriptor() protoreflect.EnumDescriptor {
	return file_v1_dictionary_common_enum_scheduler_proto_enumTypes[2].Descriptor()
}

func (SchedulerTaskPriority) Type() protoreflect.EnumType {
	return &file_v1_dictionary_common_enum_scheduler_proto_enumTypes[2]
}

func (x SchedulerTaskPriority) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SchedulerTaskPriority.Descriptor instead.
func (SchedulerTaskPriority) EnumDescriptor() ([]byte, []int) {
	return file_v1_dictionary_common_enum_scheduler_proto_rawDescGZIP(), []int{2}
}

var File_v1_dictionary_common_enum_scheduler_proto protoreflect.FileDescriptor

var file_v1_dictionary_common_enum_scheduler_proto_rawDesc = []byte{
	0x0a, 0x29, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f,
	0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x65, 0x6e, 0x75, 0x6d, 0x5f, 0x73, 0x63, 0x68, 0x65,
	0x64, 0x75, 0x6c, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x23, 0x66, 0x63, 0x70,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e,
	0x2a, 0x7e, 0x0a, 0x13, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x72, 0x54, 0x61, 0x73,
	0x6b, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x14, 0x0a, 0x10, 0x53, 0x48, 0x54, 0x53, 0x5f,
	0x55, 0x4e, 0x53, 0x50, 0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44, 0x10, 0x00, 0x12, 0x10, 0x0a,
	0x0c, 0x53, 0x48, 0x54, 0x53, 0x5f, 0x57, 0x41, 0x49, 0x54, 0x49, 0x4e, 0x47, 0x10, 0x01, 0x12,
	0x0e, 0x0a, 0x0a, 0x53, 0x48, 0x54, 0x53, 0x5f, 0x45, 0x52, 0x52, 0x4f, 0x52, 0x10, 0x02, 0x12,
	0x0f, 0x0a, 0x0b, 0x53, 0x48, 0x54, 0x53, 0x5f, 0x4c, 0x4f, 0x43, 0x4b, 0x45, 0x44, 0x10, 0x03,
	0x12, 0x0d, 0x0a, 0x09, 0x53, 0x48, 0x54, 0x53, 0x5f, 0x57, 0x4f, 0x52, 0x4b, 0x10, 0x04, 0x12,
	0x0f, 0x0a, 0x0b, 0x53, 0x48, 0x54, 0x53, 0x5f, 0x44, 0x45, 0x4c, 0x45, 0x54, 0x45, 0x10, 0x05,
	0x2a, 0x67, 0x0a, 0x11, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x72, 0x54, 0x61, 0x73,
	0x6b, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x10, 0x53, 0x48, 0x54, 0x54, 0x5f, 0x55, 0x4e,
	0x53, 0x50, 0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44, 0x10, 0x00, 0x12, 0x0d, 0x0a, 0x09, 0x53,
	0x48, 0x54, 0x54, 0x5f, 0x4f, 0x4e, 0x43, 0x45, 0x10, 0x01, 0x12, 0x11, 0x0a, 0x0d, 0x53, 0x48,
	0x54, 0x54, 0x5f, 0x50, 0x45, 0x52, 0x49, 0x4f, 0x44, 0x49, 0x43, 0x10, 0x02, 0x12, 0x1a, 0x0a,
	0x16, 0x53, 0x48, 0x54, 0x54, 0x5f, 0x50, 0x45, 0x52, 0x49, 0x4f, 0x44, 0x49, 0x43, 0x5f, 0x42,
	0x59, 0x5f, 0x4d, 0x4f, 0x44, 0x45, 0x4c, 0x10, 0x03, 0x2a, 0x85, 0x01, 0x0a, 0x15, 0x53, 0x63,
	0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x72, 0x54, 0x61, 0x73, 0x6b, 0x50, 0x72, 0x69, 0x6f, 0x72,
	0x69, 0x74, 0x79, 0x12, 0x14, 0x0a, 0x10, 0x53, 0x48, 0x54, 0x50, 0x5f, 0x55, 0x4e, 0x53, 0x50,
	0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44, 0x10, 0x00, 0x12, 0x11, 0x0a, 0x0d, 0x53, 0x48, 0x54,
	0x50, 0x5f, 0x43, 0x52, 0x49, 0x54, 0x49, 0x43, 0x41, 0x4c, 0x10, 0x01, 0x12, 0x12, 0x0a, 0x0e,
	0x53, 0x48, 0x54, 0x50, 0x5f, 0x49, 0x4d, 0x50, 0x4f, 0x52, 0x54, 0x41, 0x4e, 0x54, 0x10, 0x02,
	0x12, 0x0f, 0x0a, 0x0b, 0x53, 0x48, 0x54, 0x50, 0x5f, 0x43, 0x4f, 0x4d, 0x4d, 0x4f, 0x4e, 0x10,
	0x03, 0x12, 0x0e, 0x0a, 0x0a, 0x53, 0x48, 0x54, 0x50, 0x5f, 0x4d, 0x49, 0x4e, 0x4f, 0x52, 0x10,
	0x04, 0x12, 0x0e, 0x0a, 0x0a, 0x53, 0x48, 0x54, 0x50, 0x5f, 0x53, 0x54, 0x55, 0x46, 0x46, 0x10,
	0x05, 0x42, 0x76, 0x0a, 0x2c, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x61, 0x70, 0x69,
	0x73, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x50, 0x01, 0x5a, 0x41, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f,
	0x61, 0x64, 0x68, 0x6f, 0x63, 0x67, 0x75, 0x72, 0x75, 0x2f, 0x66, 0x63, 0x70, 0x2f, 0x61, 0x70,
	0x69, 0x73, 0x2f, 0x67, 0x65, 0x6e, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72,
	0x79, 0x2f, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f,
	0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0xba, 0x02, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x33,
}

var (
	file_v1_dictionary_common_enum_scheduler_proto_rawDescOnce sync.Once
	file_v1_dictionary_common_enum_scheduler_proto_rawDescData = file_v1_dictionary_common_enum_scheduler_proto_rawDesc
)

func file_v1_dictionary_common_enum_scheduler_proto_rawDescGZIP() []byte {
	file_v1_dictionary_common_enum_scheduler_proto_rawDescOnce.Do(func() {
		file_v1_dictionary_common_enum_scheduler_proto_rawDescData = protoimpl.X.CompressGZIP(file_v1_dictionary_common_enum_scheduler_proto_rawDescData)
	})
	return file_v1_dictionary_common_enum_scheduler_proto_rawDescData
}

var file_v1_dictionary_common_enum_scheduler_proto_enumTypes = make([]protoimpl.EnumInfo, 3)
var file_v1_dictionary_common_enum_scheduler_proto_goTypes = []interface{}{
	(SchedulerTaskStatus)(0),   // 0: fcp.dictionary.v1.dictionary_common.SchedulerTaskStatus
	(SchedulerTaskType)(0),     // 1: fcp.dictionary.v1.dictionary_common.SchedulerTaskType
	(SchedulerTaskPriority)(0), // 2: fcp.dictionary.v1.dictionary_common.SchedulerTaskPriority
}
var file_v1_dictionary_common_enum_scheduler_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_v1_dictionary_common_enum_scheduler_proto_init() }
func file_v1_dictionary_common_enum_scheduler_proto_init() {
	if File_v1_dictionary_common_enum_scheduler_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_dictionary_common_enum_scheduler_proto_rawDesc,
			NumEnums:      3,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_v1_dictionary_common_enum_scheduler_proto_goTypes,
		DependencyIndexes: file_v1_dictionary_common_enum_scheduler_proto_depIdxs,
		EnumInfos:         file_v1_dictionary_common_enum_scheduler_proto_enumTypes,
	}.Build()
	File_v1_dictionary_common_enum_scheduler_proto = out.File
	file_v1_dictionary_common_enum_scheduler_proto_rawDesc = nil
	file_v1_dictionary_common_enum_scheduler_proto_goTypes = nil
	file_v1_dictionary_common_enum_scheduler_proto_depIdxs = nil
}
