// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.13.0
// source: v1/dictionary/product_sub_group.proto

package dictionary

import (
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	wrappers "github.com/golang/protobuf/ptypes/wrappers"
	dictionary_common "gitlab.com/adhocguru/fcp/apis/gen/dictionary/v1/dictionary_common"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ProductSubGroup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        int64                 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name      string                `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Url       string                `protobuf:"bytes,3,opt,name=url,proto3" json:"url,omitempty"`
	Hash      *wrappers.StringValue `protobuf:"bytes,21,opt,name=hash,proto3" json:"hash,omitempty"`
	Deleted   *wrappers.BoolValue   `protobuf:"bytes,22,opt,name=deleted,proto3" json:"deleted,omitempty"`
	CreatedBy *wrappers.StringValue `protobuf:"bytes,23,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	CreatedAt *timestamp.Timestamp  `protobuf:"bytes,24,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedBy *wrappers.StringValue `protobuf:"bytes,25,opt,name=updated_by,json=updatedBy,proto3" json:"updated_by,omitempty"`
	UpdatedAt *timestamp.Timestamp  `protobuf:"bytes,26,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *ProductSubGroup) Reset() {
	*x = ProductSubGroup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductSubGroup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductSubGroup) ProtoMessage() {}

func (x *ProductSubGroup) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductSubGroup.ProtoReflect.Descriptor instead.
func (*ProductSubGroup) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_product_sub_group_proto_rawDescGZIP(), []int{0}
}

func (x *ProductSubGroup) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *ProductSubGroup) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *ProductSubGroup) GetUrl() string {
	if x != nil {
		return x.Url
	}
	return ""
}

func (x *ProductSubGroup) GetHash() *wrappers.StringValue {
	if x != nil {
		return x.Hash
	}
	return nil
}

func (x *ProductSubGroup) GetDeleted() *wrappers.BoolValue {
	if x != nil {
		return x.Deleted
	}
	return nil
}

func (x *ProductSubGroup) GetCreatedBy() *wrappers.StringValue {
	if x != nil {
		return x.CreatedBy
	}
	return nil
}

func (x *ProductSubGroup) GetCreatedAt() *timestamp.Timestamp {
	if x != nil {
		return x.CreatedAt
	}
	return nil
}

func (x *ProductSubGroup) GetUpdatedBy() *wrappers.StringValue {
	if x != nil {
		return x.UpdatedBy
	}
	return nil
}

func (x *ProductSubGroup) GetUpdatedAt() *timestamp.Timestamp {
	if x != nil {
		return x.UpdatedAt
	}
	return nil
}

type ProductSubGroupFilter struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id      *wrappers.Int64Value  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name    *wrappers.StringValue `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"` // LIKE
	Deleted *wrappers.BoolValue   `protobuf:"bytes,22,opt,name=deleted,proto3" json:"deleted,omitempty"`
}

func (x *ProductSubGroupFilter) Reset() {
	*x = ProductSubGroupFilter{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductSubGroupFilter) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductSubGroupFilter) ProtoMessage() {}

func (x *ProductSubGroupFilter) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductSubGroupFilter.ProtoReflect.Descriptor instead.
func (*ProductSubGroupFilter) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_product_sub_group_proto_rawDescGZIP(), []int{1}
}

func (x *ProductSubGroupFilter) GetId() *wrappers.Int64Value {
	if x != nil {
		return x.Id
	}
	return nil
}

func (x *ProductSubGroupFilter) GetName() *wrappers.StringValue {
	if x != nil {
		return x.Name
	}
	return nil
}

func (x *ProductSubGroupFilter) GetDeleted() *wrappers.BoolValue {
	if x != nil {
		return x.Deleted
	}
	return nil
}

type ListProductSubGroupRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Filter *ProductSubGroupFilter `protobuf:"bytes,1,opt,name=filter,proto3" json:"filter,omitempty"`
	// sortable fields: id, name, created_by, created_at, updated_by, updated_at
	Sortings []*dictionary_common.Sorting `protobuf:"bytes,2,rep,name=sortings,proto3" json:"sortings,omitempty"`
	// Pagination - Optional
	Pagination *dictionary_common.PaginationRequest `protobuf:"bytes,3,opt,name=pagination,proto3" json:"pagination,omitempty"`
}

func (x *ListProductSubGroupRequest) Reset() {
	*x = ListProductSubGroupRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListProductSubGroupRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListProductSubGroupRequest) ProtoMessage() {}

func (x *ListProductSubGroupRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListProductSubGroupRequest.ProtoReflect.Descriptor instead.
func (*ListProductSubGroupRequest) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_product_sub_group_proto_rawDescGZIP(), []int{2}
}

func (x *ListProductSubGroupRequest) GetFilter() *ProductSubGroupFilter {
	if x != nil {
		return x.Filter
	}
	return nil
}

func (x *ListProductSubGroupRequest) GetSortings() []*dictionary_common.Sorting {
	if x != nil {
		return x.Sortings
	}
	return nil
}

func (x *ListProductSubGroupRequest) GetPagination() *dictionary_common.PaginationRequest {
	if x != nil {
		return x.Pagination
	}
	return nil
}

type ListProductSubGroupResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Items      []*ProductSubGroup                    `protobuf:"bytes,1,rep,name=items,proto3" json:"items,omitempty"`
	Pagination *dictionary_common.PaginationResponse `protobuf:"bytes,2,opt,name=pagination,proto3" json:"pagination,omitempty"`
}

func (x *ListProductSubGroupResponse) Reset() {
	*x = ListProductSubGroupResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListProductSubGroupResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListProductSubGroupResponse) ProtoMessage() {}

func (x *ListProductSubGroupResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListProductSubGroupResponse.ProtoReflect.Descriptor instead.
func (*ListProductSubGroupResponse) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_product_sub_group_proto_rawDescGZIP(), []int{3}
}

func (x *ListProductSubGroupResponse) GetItems() []*ProductSubGroup {
	if x != nil {
		return x.Items
	}
	return nil
}

func (x *ListProductSubGroupResponse) GetPagination() *dictionary_common.PaginationResponse {
	if x != nil {
		return x.Pagination
	}
	return nil
}

type GetProductSubGroupRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetProductSubGroupRequest) Reset() {
	*x = GetProductSubGroupRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetProductSubGroupRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetProductSubGroupRequest) ProtoMessage() {}

func (x *GetProductSubGroupRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetProductSubGroupRequest.ProtoReflect.Descriptor instead.
func (*GetProductSubGroupRequest) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_product_sub_group_proto_rawDescGZIP(), []int{4}
}

func (x *GetProductSubGroupRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

type GetProductSubGroupResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Item *ProductSubGroup `protobuf:"bytes,1,opt,name=item,proto3" json:"item,omitempty"`
}

func (x *GetProductSubGroupResponse) Reset() {
	*x = GetProductSubGroupResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetProductSubGroupResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetProductSubGroupResponse) ProtoMessage() {}

func (x *GetProductSubGroupResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_product_sub_group_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetProductSubGroupResponse.ProtoReflect.Descriptor instead.
func (*GetProductSubGroupResponse) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_product_sub_group_proto_rawDescGZIP(), []int{5}
}

func (x *GetProductSubGroupResponse) GetItem() *ProductSubGroup {
	if x != nil {
		return x.Item
	}
	return nil
}

var File_v1_dictionary_product_sub_group_proto protoreflect.FileDescriptor

var file_v1_dictionary_product_sub_group_proto_rawDesc = []byte{
	0x0a, 0x25, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2f,
	0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x73, 0x75, 0x62, 0x5f, 0x67, 0x72, 0x6f, 0x75,
	0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1c, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x61, 0x72, 0x79, 0x1a, 0x1e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x73, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x2c, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0xa5, 0x03, 0x0a, 0x0f, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x10, 0x0a, 0x03,
	0x75, 0x72, 0x6c, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x72, 0x6c, 0x12, 0x30,
	0x0a, 0x04, 0x68, 0x61, 0x73, 0x68, 0x18, 0x15, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53,
	0x74, 0x72, 0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x04, 0x68, 0x61, 0x73, 0x68,
	0x12, 0x34, 0x0a, 0x07, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x18, 0x16, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x42, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x07, 0x64,
	0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x12, 0x3b, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x5f, 0x62, 0x79, 0x18, 0x17, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72,
	0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x42, 0x79, 0x12, 0x39, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61,
	0x74, 0x18, 0x18, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74,
	0x61, 0x6d, 0x70, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x3b,
	0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x62, 0x79, 0x18, 0x19, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65,
	0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x42, 0x79, 0x12, 0x39, 0x0a, 0x0a, 0x75,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x1a, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x75, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x4a, 0x04, 0x08, 0x04, 0x10, 0x15, 0x22, 0xb8, 0x01, 0x0a,
	0x15, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70,
	0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12, 0x2b, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x30, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x34, 0x0a, 0x07, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64,
	0x18, 0x16, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x42, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c,
	0x75, 0x65, 0x52, 0x07, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x4a, 0x04, 0x08, 0x03, 0x10,
	0x15, 0x4a, 0x04, 0x08, 0x15, 0x10, 0x16, 0x22, 0x8b, 0x02, 0x0a, 0x1a, 0x4c, 0x69, 0x73, 0x74,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x4b, 0x0a, 0x06, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x33, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x75, 0x62,
	0x47, 0x72, 0x6f, 0x75, 0x70, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x52, 0x06, 0x66, 0x69, 0x6c,
	0x74, 0x65, 0x72, 0x12, 0x48, 0x0a, 0x08, 0x73, 0x6f, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x18,
	0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2c, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x53, 0x6f, 0x72, 0x74,
	0x69, 0x6e, 0x67, 0x52, 0x08, 0x73, 0x6f, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x12, 0x56, 0x0a,
	0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x36, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61,
	0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79,
	0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0xbb, 0x01, 0x0a, 0x1b, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72,
	0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x43, 0x0a, 0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x2d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x61, 0x72, 0x79, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x75, 0x62, 0x47, 0x72,
	0x6f, 0x75, 0x70, 0x52, 0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x12, 0x57, 0x0a, 0x0a, 0x70, 0x61,
	0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x37,
	0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e,
	0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f,
	0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x52, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x22, 0x2b, 0x0a, 0x19, 0x47, 0x65, 0x74, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64,
	0x22, 0x5f, 0x0a, 0x1a, 0x47, 0x65, 0x74, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x75,
	0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x41,
	0x0a, 0x04, 0x69, 0x74, 0x65, 0x6d, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2d, 0x2e, 0x66,
	0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x50, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x04, 0x69, 0x74, 0x65,
	0x6d, 0x42, 0x68, 0x0a, 0x25, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x61, 0x70, 0x69,
	0x73, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x50, 0x01, 0x5a, 0x3a, 0x67, 0x69,
	0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x61, 0x64, 0x68, 0x6f, 0x63, 0x67, 0x75,
	0x72, 0x75, 0x2f, 0x66, 0x63, 0x70, 0x2f, 0x61, 0x70, 0x69, 0x73, 0x2f, 0x67, 0x65, 0x6e, 0x2f,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2f, 0x76, 0x31, 0x2f, 0x64, 0x69,
	0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0xba, 0x02, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_v1_dictionary_product_sub_group_proto_rawDescOnce sync.Once
	file_v1_dictionary_product_sub_group_proto_rawDescData = file_v1_dictionary_product_sub_group_proto_rawDesc
)

func file_v1_dictionary_product_sub_group_proto_rawDescGZIP() []byte {
	file_v1_dictionary_product_sub_group_proto_rawDescOnce.Do(func() {
		file_v1_dictionary_product_sub_group_proto_rawDescData = protoimpl.X.CompressGZIP(file_v1_dictionary_product_sub_group_proto_rawDescData)
	})
	return file_v1_dictionary_product_sub_group_proto_rawDescData
}

var file_v1_dictionary_product_sub_group_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_v1_dictionary_product_sub_group_proto_goTypes = []interface{}{
	(*ProductSubGroup)(nil),                      // 0: fcp.dictionary.v1.dictionary.ProductSubGroup
	(*ProductSubGroupFilter)(nil),                // 1: fcp.dictionary.v1.dictionary.ProductSubGroupFilter
	(*ListProductSubGroupRequest)(nil),           // 2: fcp.dictionary.v1.dictionary.ListProductSubGroupRequest
	(*ListProductSubGroupResponse)(nil),          // 3: fcp.dictionary.v1.dictionary.ListProductSubGroupResponse
	(*GetProductSubGroupRequest)(nil),            // 4: fcp.dictionary.v1.dictionary.GetProductSubGroupRequest
	(*GetProductSubGroupResponse)(nil),           // 5: fcp.dictionary.v1.dictionary.GetProductSubGroupResponse
	(*wrappers.StringValue)(nil),                 // 6: google.protobuf.StringValue
	(*wrappers.BoolValue)(nil),                   // 7: google.protobuf.BoolValue
	(*timestamp.Timestamp)(nil),                  // 8: google.protobuf.Timestamp
	(*wrappers.Int64Value)(nil),                  // 9: google.protobuf.Int64Value
	(*dictionary_common.Sorting)(nil),            // 10: fcp.dictionary.v1.dictionary_common.Sorting
	(*dictionary_common.PaginationRequest)(nil),  // 11: fcp.dictionary.v1.dictionary_common.PaginationRequest
	(*dictionary_common.PaginationResponse)(nil), // 12: fcp.dictionary.v1.dictionary_common.PaginationResponse
}
var file_v1_dictionary_product_sub_group_proto_depIdxs = []int32{
	6,  // 0: fcp.dictionary.v1.dictionary.ProductSubGroup.hash:type_name -> google.protobuf.StringValue
	7,  // 1: fcp.dictionary.v1.dictionary.ProductSubGroup.deleted:type_name -> google.protobuf.BoolValue
	6,  // 2: fcp.dictionary.v1.dictionary.ProductSubGroup.created_by:type_name -> google.protobuf.StringValue
	8,  // 3: fcp.dictionary.v1.dictionary.ProductSubGroup.created_at:type_name -> google.protobuf.Timestamp
	6,  // 4: fcp.dictionary.v1.dictionary.ProductSubGroup.updated_by:type_name -> google.protobuf.StringValue
	8,  // 5: fcp.dictionary.v1.dictionary.ProductSubGroup.updated_at:type_name -> google.protobuf.Timestamp
	9,  // 6: fcp.dictionary.v1.dictionary.ProductSubGroupFilter.id:type_name -> google.protobuf.Int64Value
	6,  // 7: fcp.dictionary.v1.dictionary.ProductSubGroupFilter.name:type_name -> google.protobuf.StringValue
	7,  // 8: fcp.dictionary.v1.dictionary.ProductSubGroupFilter.deleted:type_name -> google.protobuf.BoolValue
	1,  // 9: fcp.dictionary.v1.dictionary.ListProductSubGroupRequest.filter:type_name -> fcp.dictionary.v1.dictionary.ProductSubGroupFilter
	10, // 10: fcp.dictionary.v1.dictionary.ListProductSubGroupRequest.sortings:type_name -> fcp.dictionary.v1.dictionary_common.Sorting
	11, // 11: fcp.dictionary.v1.dictionary.ListProductSubGroupRequest.pagination:type_name -> fcp.dictionary.v1.dictionary_common.PaginationRequest
	0,  // 12: fcp.dictionary.v1.dictionary.ListProductSubGroupResponse.items:type_name -> fcp.dictionary.v1.dictionary.ProductSubGroup
	12, // 13: fcp.dictionary.v1.dictionary.ListProductSubGroupResponse.pagination:type_name -> fcp.dictionary.v1.dictionary_common.PaginationResponse
	0,  // 14: fcp.dictionary.v1.dictionary.GetProductSubGroupResponse.item:type_name -> fcp.dictionary.v1.dictionary.ProductSubGroup
	15, // [15:15] is the sub-list for method output_type
	15, // [15:15] is the sub-list for method input_type
	15, // [15:15] is the sub-list for extension type_name
	15, // [15:15] is the sub-list for extension extendee
	0,  // [0:15] is the sub-list for field type_name
}

func init() { file_v1_dictionary_product_sub_group_proto_init() }
func file_v1_dictionary_product_sub_group_proto_init() {
	if File_v1_dictionary_product_sub_group_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_v1_dictionary_product_sub_group_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductSubGroup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_product_sub_group_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductSubGroupFilter); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_product_sub_group_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListProductSubGroupRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_product_sub_group_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListProductSubGroupResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_product_sub_group_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetProductSubGroupRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_product_sub_group_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetProductSubGroupResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_dictionary_product_sub_group_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_v1_dictionary_product_sub_group_proto_goTypes,
		DependencyIndexes: file_v1_dictionary_product_sub_group_proto_depIdxs,
		MessageInfos:      file_v1_dictionary_product_sub_group_proto_msgTypes,
	}.Build()
	File_v1_dictionary_product_sub_group_proto = out.File
	file_v1_dictionary_product_sub_group_proto_rawDesc = nil
	file_v1_dictionary_product_sub_group_proto_goTypes = nil
	file_v1_dictionary_product_sub_group_proto_depIdxs = nil
}
