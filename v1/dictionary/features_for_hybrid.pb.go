// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.13.0
// source: v1/dictionary/features_for_hybrid.proto

package dictionary

import (
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	wrappers "github.com/golang/protobuf/ptypes/wrappers"
	dictionary_common "gitlab.com/adhocguru/fcp/apis/gen/dictionary/v1/dictionary_common"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type FeaturesForHybrid struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                int64                 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	FeatureId         int64                 `protobuf:"varint,2,opt,name=feature_id,json=featureId,proto3" json:"feature_id,omitempty"`
	FeatureName       string                `protobuf:"bytes,3,opt,name=feature_name,json=featureName,proto3" json:"feature_name,omitempty"`
	HybridId          int64                 `protobuf:"varint,4,opt,name=hybrid_id,json=hybridId,proto3" json:"hybrid_id,omitempty"`
	HybridName        string                `protobuf:"bytes,5,opt,name=hybrid_name,json=hybridName,proto3" json:"hybrid_name,omitempty"`
	FeaturesValueId   int64                 `protobuf:"varint,6,opt,name=features_value_id,json=featuresValueId,proto3" json:"features_value_id,omitempty"`
	FeaturesHardValue *wrappers.Int64Value  `protobuf:"bytes,7,opt,name=features_hard_value,json=featuresHardValue,proto3" json:"features_hard_value,omitempty"`
	Hash              *wrappers.StringValue `protobuf:"bytes,21,opt,name=hash,proto3" json:"hash,omitempty"`
	Deleted           *wrappers.BoolValue   `protobuf:"bytes,22,opt,name=deleted,proto3" json:"deleted,omitempty"`
	CreatedBy         *wrappers.StringValue `protobuf:"bytes,23,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	CreatedAt         *timestamp.Timestamp  `protobuf:"bytes,24,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedBy         *wrappers.StringValue `protobuf:"bytes,25,opt,name=updated_by,json=updatedBy,proto3" json:"updated_by,omitempty"`
	UpdatedAt         *timestamp.Timestamp  `protobuf:"bytes,26,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *FeaturesForHybrid) Reset() {
	*x = FeaturesForHybrid{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FeaturesForHybrid) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FeaturesForHybrid) ProtoMessage() {}

func (x *FeaturesForHybrid) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FeaturesForHybrid.ProtoReflect.Descriptor instead.
func (*FeaturesForHybrid) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_features_for_hybrid_proto_rawDescGZIP(), []int{0}
}

func (x *FeaturesForHybrid) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *FeaturesForHybrid) GetFeatureId() int64 {
	if x != nil {
		return x.FeatureId
	}
	return 0
}

func (x *FeaturesForHybrid) GetFeatureName() string {
	if x != nil {
		return x.FeatureName
	}
	return ""
}

func (x *FeaturesForHybrid) GetHybridId() int64 {
	if x != nil {
		return x.HybridId
	}
	return 0
}

func (x *FeaturesForHybrid) GetHybridName() string {
	if x != nil {
		return x.HybridName
	}
	return ""
}

func (x *FeaturesForHybrid) GetFeaturesValueId() int64 {
	if x != nil {
		return x.FeaturesValueId
	}
	return 0
}

func (x *FeaturesForHybrid) GetFeaturesHardValue() *wrappers.Int64Value {
	if x != nil {
		return x.FeaturesHardValue
	}
	return nil
}

func (x *FeaturesForHybrid) GetHash() *wrappers.StringValue {
	if x != nil {
		return x.Hash
	}
	return nil
}

func (x *FeaturesForHybrid) GetDeleted() *wrappers.BoolValue {
	if x != nil {
		return x.Deleted
	}
	return nil
}

func (x *FeaturesForHybrid) GetCreatedBy() *wrappers.StringValue {
	if x != nil {
		return x.CreatedBy
	}
	return nil
}

func (x *FeaturesForHybrid) GetCreatedAt() *timestamp.Timestamp {
	if x != nil {
		return x.CreatedAt
	}
	return nil
}

func (x *FeaturesForHybrid) GetUpdatedBy() *wrappers.StringValue {
	if x != nil {
		return x.UpdatedBy
	}
	return nil
}

func (x *FeaturesForHybrid) GetUpdatedAt() *timestamp.Timestamp {
	if x != nil {
		return x.UpdatedAt
	}
	return nil
}

type FeaturesForHybridFilter struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id              *wrappers.Int64Value `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	FeatureId       *wrappers.Int64Value `protobuf:"bytes,2,opt,name=feature_id,json=featureId,proto3" json:"feature_id,omitempty"`
	HybridId        *wrappers.Int64Value `protobuf:"bytes,4,opt,name=hybrid_id,json=hybridId,proto3" json:"hybrid_id,omitempty"`
	FeaturesValueId *wrappers.Int64Value `protobuf:"bytes,6,opt,name=features_value_id,json=featuresValueId,proto3" json:"features_value_id,omitempty"`
	Deleted         *wrappers.BoolValue  `protobuf:"bytes,22,opt,name=deleted,proto3" json:"deleted,omitempty"`
}

func (x *FeaturesForHybridFilter) Reset() {
	*x = FeaturesForHybridFilter{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FeaturesForHybridFilter) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FeaturesForHybridFilter) ProtoMessage() {}

func (x *FeaturesForHybridFilter) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FeaturesForHybridFilter.ProtoReflect.Descriptor instead.
func (*FeaturesForHybridFilter) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_features_for_hybrid_proto_rawDescGZIP(), []int{1}
}

func (x *FeaturesForHybridFilter) GetId() *wrappers.Int64Value {
	if x != nil {
		return x.Id
	}
	return nil
}

func (x *FeaturesForHybridFilter) GetFeatureId() *wrappers.Int64Value {
	if x != nil {
		return x.FeatureId
	}
	return nil
}

func (x *FeaturesForHybridFilter) GetHybridId() *wrappers.Int64Value {
	if x != nil {
		return x.HybridId
	}
	return nil
}

func (x *FeaturesForHybridFilter) GetFeaturesValueId() *wrappers.Int64Value {
	if x != nil {
		return x.FeaturesValueId
	}
	return nil
}

func (x *FeaturesForHybridFilter) GetDeleted() *wrappers.BoolValue {
	if x != nil {
		return x.Deleted
	}
	return nil
}

type ListFeaturesForHybridRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Filter *FeaturesForHybridFilter `protobuf:"bytes,1,opt,name=filter,proto3" json:"filter,omitempty"`
	// sortable fields: id, created_by, created_at, updated_by, updated_at
	Sortings []*dictionary_common.Sorting `protobuf:"bytes,2,rep,name=sortings,proto3" json:"sortings,omitempty"`
	// Pagination - Optional
	Pagination *dictionary_common.PaginationRequest `protobuf:"bytes,3,opt,name=pagination,proto3" json:"pagination,omitempty"`
}

func (x *ListFeaturesForHybridRequest) Reset() {
	*x = ListFeaturesForHybridRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListFeaturesForHybridRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListFeaturesForHybridRequest) ProtoMessage() {}

func (x *ListFeaturesForHybridRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListFeaturesForHybridRequest.ProtoReflect.Descriptor instead.
func (*ListFeaturesForHybridRequest) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_features_for_hybrid_proto_rawDescGZIP(), []int{2}
}

func (x *ListFeaturesForHybridRequest) GetFilter() *FeaturesForHybridFilter {
	if x != nil {
		return x.Filter
	}
	return nil
}

func (x *ListFeaturesForHybridRequest) GetSortings() []*dictionary_common.Sorting {
	if x != nil {
		return x.Sortings
	}
	return nil
}

func (x *ListFeaturesForHybridRequest) GetPagination() *dictionary_common.PaginationRequest {
	if x != nil {
		return x.Pagination
	}
	return nil
}

type ListFeaturesForHybridResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Items      []*FeaturesForHybrid                  `protobuf:"bytes,1,rep,name=items,proto3" json:"items,omitempty"`
	Pagination *dictionary_common.PaginationResponse `protobuf:"bytes,2,opt,name=pagination,proto3" json:"pagination,omitempty"`
}

func (x *ListFeaturesForHybridResponse) Reset() {
	*x = ListFeaturesForHybridResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListFeaturesForHybridResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListFeaturesForHybridResponse) ProtoMessage() {}

func (x *ListFeaturesForHybridResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListFeaturesForHybridResponse.ProtoReflect.Descriptor instead.
func (*ListFeaturesForHybridResponse) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_features_for_hybrid_proto_rawDescGZIP(), []int{3}
}

func (x *ListFeaturesForHybridResponse) GetItems() []*FeaturesForHybrid {
	if x != nil {
		return x.Items
	}
	return nil
}

func (x *ListFeaturesForHybridResponse) GetPagination() *dictionary_common.PaginationResponse {
	if x != nil {
		return x.Pagination
	}
	return nil
}

type GetFeaturesForHybridRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetFeaturesForHybridRequest) Reset() {
	*x = GetFeaturesForHybridRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetFeaturesForHybridRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetFeaturesForHybridRequest) ProtoMessage() {}

func (x *GetFeaturesForHybridRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetFeaturesForHybridRequest.ProtoReflect.Descriptor instead.
func (*GetFeaturesForHybridRequest) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_features_for_hybrid_proto_rawDescGZIP(), []int{4}
}

func (x *GetFeaturesForHybridRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

type GetFeaturesForHybridResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Item *FeaturesForHybrid `protobuf:"bytes,1,opt,name=item,proto3" json:"item,omitempty"`
}

func (x *GetFeaturesForHybridResponse) Reset() {
	*x = GetFeaturesForHybridResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetFeaturesForHybridResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetFeaturesForHybridResponse) ProtoMessage() {}

func (x *GetFeaturesForHybridResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_features_for_hybrid_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetFeaturesForHybridResponse.ProtoReflect.Descriptor instead.
func (*GetFeaturesForHybridResponse) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_features_for_hybrid_proto_rawDescGZIP(), []int{5}
}

func (x *GetFeaturesForHybridResponse) GetItem() *FeaturesForHybrid {
	if x != nil {
		return x.Item
	}
	return nil
}

var File_v1_dictionary_features_for_hybrid_proto protoreflect.FileDescriptor

var file_v1_dictionary_features_for_hybrid_proto_rawDesc = []byte{
	0x0a, 0x27, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2f,
	0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x5f, 0x66, 0x6f, 0x72, 0x5f, 0x68, 0x79, 0x62,
	0x72, 0x69, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1c, 0x66, 0x63, 0x70, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x1a, 0x1e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72,
	0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61,
	0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x2c, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xfa, 0x04, 0x0a, 0x11, 0x46, 0x65, 0x61, 0x74, 0x75,
	0x72, 0x65, 0x73, 0x46, 0x6f, 0x72, 0x48, 0x79, 0x62, 0x72, 0x69, 0x64, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1d, 0x0a, 0x0a,
	0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x09, 0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x49, 0x64, 0x12, 0x21, 0x0a, 0x0c, 0x66,
	0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x1b,
	0x0a, 0x09, 0x68, 0x79, 0x62, 0x72, 0x69, 0x64, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x08, 0x68, 0x79, 0x62, 0x72, 0x69, 0x64, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x68,
	0x79, 0x62, 0x72, 0x69, 0x64, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0a, 0x68, 0x79, 0x62, 0x72, 0x69, 0x64, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x2a, 0x0a, 0x11,
	0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x5f, 0x69,
	0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0f, 0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65,
	0x73, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x49, 0x64, 0x12, 0x4b, 0x0a, 0x13, 0x66, 0x65, 0x61, 0x74,
	0x75, 0x72, 0x65, 0x73, 0x5f, 0x68, 0x61, 0x72, 0x64, 0x5f, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x56, 0x61, 0x6c,
	0x75, 0x65, 0x52, 0x11, 0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x48, 0x61, 0x72, 0x64,
	0x56, 0x61, 0x6c, 0x75, 0x65, 0x12, 0x30, 0x0a, 0x04, 0x68, 0x61, 0x73, 0x68, 0x18, 0x15, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75,
	0x65, 0x52, 0x04, 0x68, 0x61, 0x73, 0x68, 0x12, 0x34, 0x0a, 0x07, 0x64, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x64, 0x18, 0x16, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x42, 0x6f, 0x6f, 0x6c, 0x56,
	0x61, 0x6c, 0x75, 0x65, 0x52, 0x07, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x12, 0x3b, 0x0a,
	0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x62, 0x79, 0x18, 0x17, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52,
	0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x42, 0x79, 0x12, 0x39, 0x0a, 0x0a, 0x63, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x18, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a,
	0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66,
	0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x3b, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64,
	0x5f, 0x62, 0x79, 0x18, 0x19, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69,
	0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64,
	0x42, 0x79, 0x12, 0x39, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x1a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61,
	0x6d, 0x70, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x4a, 0x04, 0x08,
	0x08, 0x10, 0x15, 0x22, 0xd3, 0x02, 0x0a, 0x17, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73,
	0x46, 0x6f, 0x72, 0x48, 0x79, 0x62, 0x72, 0x69, 0x64, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12,
	0x2b, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x49, 0x6e,
	0x74, 0x36, 0x34, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x02, 0x69, 0x64, 0x12, 0x3a, 0x0a, 0x0a,
	0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x1b, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2e, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x09, 0x66,
	0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x49, 0x64, 0x12, 0x38, 0x0a, 0x09, 0x68, 0x79, 0x62, 0x72,
	0x69, 0x64, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x49, 0x6e,
	0x74, 0x36, 0x34, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x08, 0x68, 0x79, 0x62, 0x72, 0x69, 0x64,
	0x49, 0x64, 0x12, 0x47, 0x0a, 0x11, 0x66, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x5f, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e,
	0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e,
	0x49, 0x6e, 0x74, 0x36, 0x34, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x0f, 0x66, 0x65, 0x61, 0x74,
	0x75, 0x72, 0x65, 0x73, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x49, 0x64, 0x12, 0x34, 0x0a, 0x07, 0x64,
	0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x18, 0x16, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x42,
	0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x07, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65,
	0x64, 0x4a, 0x04, 0x08, 0x03, 0x10, 0x04, 0x4a, 0x04, 0x08, 0x05, 0x10, 0x06, 0x4a, 0x04, 0x08,
	0x07, 0x10, 0x15, 0x4a, 0x04, 0x08, 0x15, 0x10, 0x16, 0x22, 0x8f, 0x02, 0x0a, 0x1c, 0x4c, 0x69,
	0x73, 0x74, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x46, 0x6f, 0x72, 0x48, 0x79, 0x62,
	0x72, 0x69, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x4d, 0x0a, 0x06, 0x66, 0x69,
	0x6c, 0x74, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x35, 0x2e, 0x66, 0x63, 0x70,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72,
	0x65, 0x73, 0x46, 0x6f, 0x72, 0x48, 0x79, 0x62, 0x72, 0x69, 0x64, 0x46, 0x69, 0x6c, 0x74, 0x65,
	0x72, 0x52, 0x06, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12, 0x48, 0x0a, 0x08, 0x73, 0x6f, 0x72,
	0x74, 0x69, 0x6e, 0x67, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2c, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2e, 0x53, 0x6f, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x52, 0x08, 0x73, 0x6f, 0x72, 0x74, 0x69,
	0x6e, 0x67, 0x73, 0x12, 0x56, 0x0a, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x36, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69,
	0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x50, 0x61,
	0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52,
	0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0xbf, 0x01, 0x0a, 0x1d,
	0x4c, 0x69, 0x73, 0x74, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x46, 0x6f, 0x72, 0x48,
	0x79, 0x62, 0x72, 0x69, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x45, 0x0a,
	0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2f, 0x2e, 0x66,
	0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x46, 0x65, 0x61, 0x74,
	0x75, 0x72, 0x65, 0x73, 0x46, 0x6f, 0x72, 0x48, 0x79, 0x62, 0x72, 0x69, 0x64, 0x52, 0x05, 0x69,
	0x74, 0x65, 0x6d, 0x73, 0x12, 0x57, 0x0a, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x37, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x50,
	0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x52, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x2d, 0x0a,
	0x1b, 0x47, 0x65, 0x74, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x46, 0x6f, 0x72, 0x48,
	0x79, 0x62, 0x72, 0x69, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x22, 0x63, 0x0a, 0x1c,
	0x47, 0x65, 0x74, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x73, 0x46, 0x6f, 0x72, 0x48, 0x79,
	0x62, 0x72, 0x69, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x43, 0x0a, 0x04,
	0x69, 0x74, 0x65, 0x6d, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2f, 0x2e, 0x66, 0x63, 0x70,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72,
	0x65, 0x73, 0x46, 0x6f, 0x72, 0x48, 0x79, 0x62, 0x72, 0x69, 0x64, 0x52, 0x04, 0x69, 0x74, 0x65,
	0x6d, 0x42, 0x68, 0x0a, 0x25, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x61, 0x70, 0x69,
	0x73, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x50, 0x01, 0x5a, 0x3a, 0x67, 0x69,
	0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x61, 0x64, 0x68, 0x6f, 0x63, 0x67, 0x75,
	0x72, 0x75, 0x2f, 0x66, 0x63, 0x70, 0x2f, 0x61, 0x70, 0x69, 0x73, 0x2f, 0x67, 0x65, 0x6e, 0x2f,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2f, 0x76, 0x31, 0x2f, 0x64, 0x69,
	0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0xba, 0x02, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_v1_dictionary_features_for_hybrid_proto_rawDescOnce sync.Once
	file_v1_dictionary_features_for_hybrid_proto_rawDescData = file_v1_dictionary_features_for_hybrid_proto_rawDesc
)

func file_v1_dictionary_features_for_hybrid_proto_rawDescGZIP() []byte {
	file_v1_dictionary_features_for_hybrid_proto_rawDescOnce.Do(func() {
		file_v1_dictionary_features_for_hybrid_proto_rawDescData = protoimpl.X.CompressGZIP(file_v1_dictionary_features_for_hybrid_proto_rawDescData)
	})
	return file_v1_dictionary_features_for_hybrid_proto_rawDescData
}

var file_v1_dictionary_features_for_hybrid_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_v1_dictionary_features_for_hybrid_proto_goTypes = []interface{}{
	(*FeaturesForHybrid)(nil),                    // 0: fcp.dictionary.v1.dictionary.FeaturesForHybrid
	(*FeaturesForHybridFilter)(nil),              // 1: fcp.dictionary.v1.dictionary.FeaturesForHybridFilter
	(*ListFeaturesForHybridRequest)(nil),         // 2: fcp.dictionary.v1.dictionary.ListFeaturesForHybridRequest
	(*ListFeaturesForHybridResponse)(nil),        // 3: fcp.dictionary.v1.dictionary.ListFeaturesForHybridResponse
	(*GetFeaturesForHybridRequest)(nil),          // 4: fcp.dictionary.v1.dictionary.GetFeaturesForHybridRequest
	(*GetFeaturesForHybridResponse)(nil),         // 5: fcp.dictionary.v1.dictionary.GetFeaturesForHybridResponse
	(*wrappers.Int64Value)(nil),                  // 6: google.protobuf.Int64Value
	(*wrappers.StringValue)(nil),                 // 7: google.protobuf.StringValue
	(*wrappers.BoolValue)(nil),                   // 8: google.protobuf.BoolValue
	(*timestamp.Timestamp)(nil),                  // 9: google.protobuf.Timestamp
	(*dictionary_common.Sorting)(nil),            // 10: fcp.dictionary.v1.dictionary_common.Sorting
	(*dictionary_common.PaginationRequest)(nil),  // 11: fcp.dictionary.v1.dictionary_common.PaginationRequest
	(*dictionary_common.PaginationResponse)(nil), // 12: fcp.dictionary.v1.dictionary_common.PaginationResponse
}
var file_v1_dictionary_features_for_hybrid_proto_depIdxs = []int32{
	6,  // 0: fcp.dictionary.v1.dictionary.FeaturesForHybrid.features_hard_value:type_name -> google.protobuf.Int64Value
	7,  // 1: fcp.dictionary.v1.dictionary.FeaturesForHybrid.hash:type_name -> google.protobuf.StringValue
	8,  // 2: fcp.dictionary.v1.dictionary.FeaturesForHybrid.deleted:type_name -> google.protobuf.BoolValue
	7,  // 3: fcp.dictionary.v1.dictionary.FeaturesForHybrid.created_by:type_name -> google.protobuf.StringValue
	9,  // 4: fcp.dictionary.v1.dictionary.FeaturesForHybrid.created_at:type_name -> google.protobuf.Timestamp
	7,  // 5: fcp.dictionary.v1.dictionary.FeaturesForHybrid.updated_by:type_name -> google.protobuf.StringValue
	9,  // 6: fcp.dictionary.v1.dictionary.FeaturesForHybrid.updated_at:type_name -> google.protobuf.Timestamp
	6,  // 7: fcp.dictionary.v1.dictionary.FeaturesForHybridFilter.id:type_name -> google.protobuf.Int64Value
	6,  // 8: fcp.dictionary.v1.dictionary.FeaturesForHybridFilter.feature_id:type_name -> google.protobuf.Int64Value
	6,  // 9: fcp.dictionary.v1.dictionary.FeaturesForHybridFilter.hybrid_id:type_name -> google.protobuf.Int64Value
	6,  // 10: fcp.dictionary.v1.dictionary.FeaturesForHybridFilter.features_value_id:type_name -> google.protobuf.Int64Value
	8,  // 11: fcp.dictionary.v1.dictionary.FeaturesForHybridFilter.deleted:type_name -> google.protobuf.BoolValue
	1,  // 12: fcp.dictionary.v1.dictionary.ListFeaturesForHybridRequest.filter:type_name -> fcp.dictionary.v1.dictionary.FeaturesForHybridFilter
	10, // 13: fcp.dictionary.v1.dictionary.ListFeaturesForHybridRequest.sortings:type_name -> fcp.dictionary.v1.dictionary_common.Sorting
	11, // 14: fcp.dictionary.v1.dictionary.ListFeaturesForHybridRequest.pagination:type_name -> fcp.dictionary.v1.dictionary_common.PaginationRequest
	0,  // 15: fcp.dictionary.v1.dictionary.ListFeaturesForHybridResponse.items:type_name -> fcp.dictionary.v1.dictionary.FeaturesForHybrid
	12, // 16: fcp.dictionary.v1.dictionary.ListFeaturesForHybridResponse.pagination:type_name -> fcp.dictionary.v1.dictionary_common.PaginationResponse
	0,  // 17: fcp.dictionary.v1.dictionary.GetFeaturesForHybridResponse.item:type_name -> fcp.dictionary.v1.dictionary.FeaturesForHybrid
	18, // [18:18] is the sub-list for method output_type
	18, // [18:18] is the sub-list for method input_type
	18, // [18:18] is the sub-list for extension type_name
	18, // [18:18] is the sub-list for extension extendee
	0,  // [0:18] is the sub-list for field type_name
}

func init() { file_v1_dictionary_features_for_hybrid_proto_init() }
func file_v1_dictionary_features_for_hybrid_proto_init() {
	if File_v1_dictionary_features_for_hybrid_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_v1_dictionary_features_for_hybrid_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FeaturesForHybrid); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_features_for_hybrid_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FeaturesForHybridFilter); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_features_for_hybrid_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListFeaturesForHybridRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_features_for_hybrid_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListFeaturesForHybridResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_features_for_hybrid_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetFeaturesForHybridRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_features_for_hybrid_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetFeaturesForHybridResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_dictionary_features_for_hybrid_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_v1_dictionary_features_for_hybrid_proto_goTypes,
		DependencyIndexes: file_v1_dictionary_features_for_hybrid_proto_depIdxs,
		MessageInfos:      file_v1_dictionary_features_for_hybrid_proto_msgTypes,
	}.Build()
	File_v1_dictionary_features_for_hybrid_proto = out.File
	file_v1_dictionary_features_for_hybrid_proto_rawDesc = nil
	file_v1_dictionary_features_for_hybrid_proto_goTypes = nil
	file_v1_dictionary_features_for_hybrid_proto_depIdxs = nil
}
