// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package dictionary

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CountryServiceClient is the client API for CountryService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CountryServiceClient interface {
	ListCountry(ctx context.Context, in *ListCountryRequest, opts ...grpc.CallOption) (*ListCountryResponse, error)
	GetCountry(ctx context.Context, in *GetCountryRequest, opts ...grpc.CallOption) (*GetCountryResponse, error)
	CreateCountry(ctx context.Context, in *CreateCountryRequest, opts ...grpc.CallOption) (*CreateCountryResponse, error)
	UpdateCountry(ctx context.Context, in *UpdateCountryRequest, opts ...grpc.CallOption) (*UpdateCountryResponse, error)
	// Mark as deleteted record
	// and will mark as deleted related records in (retail/retail-location/v1/region)
	DeleteCountry(ctx context.Context, in *DeleteCountryRequest, opts ...grpc.CallOption) (*empty.Empty, error)
	// Mark as undeleted record
	RestoreCountry(ctx context.Context, in *RestoreCountryRequest, opts ...grpc.CallOption) (*empty.Empty, error)
}

type countryServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCountryServiceClient(cc grpc.ClientConnInterface) CountryServiceClient {
	return &countryServiceClient{cc}
}

func (c *countryServiceClient) ListCountry(ctx context.Context, in *ListCountryRequest, opts ...grpc.CallOption) (*ListCountryResponse, error) {
	out := new(ListCountryResponse)
	err := c.cc.Invoke(ctx, "/fcp.dictionary.v1.dictionary.CountryService/ListCountry", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *countryServiceClient) GetCountry(ctx context.Context, in *GetCountryRequest, opts ...grpc.CallOption) (*GetCountryResponse, error) {
	out := new(GetCountryResponse)
	err := c.cc.Invoke(ctx, "/fcp.dictionary.v1.dictionary.CountryService/GetCountry", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *countryServiceClient) CreateCountry(ctx context.Context, in *CreateCountryRequest, opts ...grpc.CallOption) (*CreateCountryResponse, error) {
	out := new(CreateCountryResponse)
	err := c.cc.Invoke(ctx, "/fcp.dictionary.v1.dictionary.CountryService/CreateCountry", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *countryServiceClient) UpdateCountry(ctx context.Context, in *UpdateCountryRequest, opts ...grpc.CallOption) (*UpdateCountryResponse, error) {
	out := new(UpdateCountryResponse)
	err := c.cc.Invoke(ctx, "/fcp.dictionary.v1.dictionary.CountryService/UpdateCountry", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *countryServiceClient) DeleteCountry(ctx context.Context, in *DeleteCountryRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/fcp.dictionary.v1.dictionary.CountryService/DeleteCountry", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *countryServiceClient) RestoreCountry(ctx context.Context, in *RestoreCountryRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/fcp.dictionary.v1.dictionary.CountryService/RestoreCountry", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CountryServiceServer is the server API for CountryService service.
// All implementations must embed UnimplementedCountryServiceServer
// for forward compatibility
type CountryServiceServer interface {
	ListCountry(context.Context, *ListCountryRequest) (*ListCountryResponse, error)
	GetCountry(context.Context, *GetCountryRequest) (*GetCountryResponse, error)
	CreateCountry(context.Context, *CreateCountryRequest) (*CreateCountryResponse, error)
	UpdateCountry(context.Context, *UpdateCountryRequest) (*UpdateCountryResponse, error)
	// Mark as deleteted record
	// and will mark as deleted related records in (retail/retail-location/v1/region)
	DeleteCountry(context.Context, *DeleteCountryRequest) (*empty.Empty, error)
	// Mark as undeleted record
	RestoreCountry(context.Context, *RestoreCountryRequest) (*empty.Empty, error)
	mustEmbedUnimplementedCountryServiceServer()
}

// UnimplementedCountryServiceServer must be embedded to have forward compatible implementations.
type UnimplementedCountryServiceServer struct {
}

func (UnimplementedCountryServiceServer) ListCountry(context.Context, *ListCountryRequest) (*ListCountryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListCountry not implemented")
}
func (UnimplementedCountryServiceServer) GetCountry(context.Context, *GetCountryRequest) (*GetCountryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCountry not implemented")
}
func (UnimplementedCountryServiceServer) CreateCountry(context.Context, *CreateCountryRequest) (*CreateCountryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateCountry not implemented")
}
func (UnimplementedCountryServiceServer) UpdateCountry(context.Context, *UpdateCountryRequest) (*UpdateCountryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateCountry not implemented")
}
func (UnimplementedCountryServiceServer) DeleteCountry(context.Context, *DeleteCountryRequest) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteCountry not implemented")
}
func (UnimplementedCountryServiceServer) RestoreCountry(context.Context, *RestoreCountryRequest) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RestoreCountry not implemented")
}
func (UnimplementedCountryServiceServer) mustEmbedUnimplementedCountryServiceServer() {}

// UnsafeCountryServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CountryServiceServer will
// result in compilation errors.
type UnsafeCountryServiceServer interface {
	mustEmbedUnimplementedCountryServiceServer()
}

func RegisterCountryServiceServer(s grpc.ServiceRegistrar, srv CountryServiceServer) {
	s.RegisterService(&CountryService_ServiceDesc, srv)
}

func _CountryService_ListCountry_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListCountryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CountryServiceServer).ListCountry(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.dictionary.v1.dictionary.CountryService/ListCountry",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CountryServiceServer).ListCountry(ctx, req.(*ListCountryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CountryService_GetCountry_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCountryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CountryServiceServer).GetCountry(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.dictionary.v1.dictionary.CountryService/GetCountry",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CountryServiceServer).GetCountry(ctx, req.(*GetCountryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CountryService_CreateCountry_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateCountryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CountryServiceServer).CreateCountry(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.dictionary.v1.dictionary.CountryService/CreateCountry",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CountryServiceServer).CreateCountry(ctx, req.(*CreateCountryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CountryService_UpdateCountry_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateCountryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CountryServiceServer).UpdateCountry(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.dictionary.v1.dictionary.CountryService/UpdateCountry",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CountryServiceServer).UpdateCountry(ctx, req.(*UpdateCountryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CountryService_DeleteCountry_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteCountryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CountryServiceServer).DeleteCountry(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.dictionary.v1.dictionary.CountryService/DeleteCountry",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CountryServiceServer).DeleteCountry(ctx, req.(*DeleteCountryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CountryService_RestoreCountry_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RestoreCountryRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CountryServiceServer).RestoreCountry(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.dictionary.v1.dictionary.CountryService/RestoreCountry",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CountryServiceServer).RestoreCountry(ctx, req.(*RestoreCountryRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// CountryService_ServiceDesc is the grpc.ServiceDesc for CountryService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CountryService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "fcp.dictionary.v1.dictionary.CountryService",
	HandlerType: (*CountryServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListCountry",
			Handler:    _CountryService_ListCountry_Handler,
		},
		{
			MethodName: "GetCountry",
			Handler:    _CountryService_GetCountry_Handler,
		},
		{
			MethodName: "CreateCountry",
			Handler:    _CountryService_CreateCountry_Handler,
		},
		{
			MethodName: "UpdateCountry",
			Handler:    _CountryService_UpdateCountry_Handler,
		},
		{
			MethodName: "DeleteCountry",
			Handler:    _CountryService_DeleteCountry_Handler,
		},
		{
			MethodName: "RestoreCountry",
			Handler:    _CountryService_RestoreCountry_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "v1/dictionary/country.proto",
}
